//
//  RWAVPlayerView.h
//  VideoPlayer
//
//  Created by Camilo Rodriguez Gaviria on 12/4/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class AVPlayer;

/**
 * A RWAVPlayerView provides a basic asset player, whitout controls nor any means of setting the content. Use this class as the basic view of a controller that will set the URL content whith the initWithURL method of the AVPlayer class.
*/
@interface RWAVPlayerView : UIView

/// The player of the view.
@property (strong, nonatomic) AVPlayer *player;

/**
 * Enables or disable the external playback, based on a boolean passed as parameter.
 *
 * @param externalPlaybackMode A boolean indicating whether the external playback should be enabled.
*/
- (void)setExternalPlayback:(BOOL)externalPlaybackMode;

@end
