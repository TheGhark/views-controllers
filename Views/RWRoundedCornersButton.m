//
//  RWRoundedCornersButton.m
//  FirstCut Pro
//
//  Created by Camilo Rodriguez Gaviria on 11/29/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import "RWRoundedCornersButton.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"

@interface RWRoundedCornersButton ()

/// The border width. It is equal to the 5% of the width.
@property (assign, nonatomic, readonly) NSInteger borderWidth;

@end

@implementation RWRoundedCornersButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat diameter = (CGRectGetHeight(frame) > CGRectGetWidth(frame)) ? CGRectGetHeight(frame) : CGRectGetWidth(frame);
        [self roundedCornersRadius:diameter/2];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        CGFloat diameter = CGRectGetWidth(self.frame);
        
        if (CGRectGetHeight(self.frame) != CGRectGetWidth(self.frame)) {
            NSLog(@"The button's frame is not square shaped. It has to have the same height and width in order to properly display a rounded button.");
        }
        
        [self roundedCornersRadius:diameter/2];
        self.clearTitle = YES;
        self.clearBackground = YES;
        _borderWidth = (int)(CGRectGetWidth(self.frame)*0.05);
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.borderColorForNormalState = (self.borderColorForNormalState) ? self.borderColorForNormalState : [UIColor lightGrayColor];
    self.borderColorForSelectedState = (self.borderColorForSelectedState) ? self.borderColorForSelectedState : [UIColor blueColor];
    UIColor *borderColor = (self.highlighted || self.selected) ? self.borderColorForSelectedState : self.borderColorForNormalState;
    [self revealBordersWithColor:borderColor width:self.borderWidth];
    
    if (self.clearBackground) {
        self.backgroundColor = [UIColor clearColor];
    }
    
    if (self.clearTitle) {
        [self setTitle:@"" forState:UIControlStateNormal];
        [self setTitle:@"" forState:UIControlStateSelected];
        [self setTitle:@"" forState:UIControlStateHighlighted];
    }
}

@end
