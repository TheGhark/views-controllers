//
//  RWRoundedCornersButton.h
//  FirstCut Pro
//
//  Created by Camilo Rodriguez Gaviria on 11/29/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import <UIKit/UIKit.h>

/// A RWRoundedCornersButton provides a button with rounded corners and with borders equals to the 5% of the width. If the width and the height are the same, the button has a circular shaped.
@interface RWRoundedCornersButton : UIButton

/// The color for the border for the normal state. The default is lightGray.
@property (strong, nonatomic) UIColor *borderColorForNormalState;

/// The color for the border for the selected and highlighted state. The default is blueColor.
@property (strong, nonatomic) UIColor *borderColorForSelectedState;

/// Indicates whether the background should be cleared. The default is set to yes.
@property (assign, nonatomic) BOOL clearBackground;

/// Indicates whether the title of the button should be cleared. The default is set to yes.
@property (assign, nonatomic) BOOL clearTitle;

@end
