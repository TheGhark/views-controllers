//
//  RWAVPlayerView.m
//  VideoPlayer
//
//  Created by Camilo Rodriguez Gaviria on 12/4/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import "RWAVPlayerView.h"

@implementation RWAVPlayerView

#pragma mark - Getters

+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (AVPlayer *)player {
    return [(AVPlayerLayer *)[self layer] player];
}

#pragma mark - Setters

- (void)setPlayer:(AVPlayer *)player {
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

#pragma mark - Public

- (void)setExternalPlayback:(BOOL)externalPlaybackMode {
    self.player.allowsExternalPlayback = externalPlaybackMode;
}

@end
