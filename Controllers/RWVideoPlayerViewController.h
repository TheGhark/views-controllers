//
//  RWVideoPlayerViewController.h
//  VideoPlayer
//
//  Created by Camilo Rodriguez Gaviria on 12/4/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "RWAVPlayerView.h"

/// Defines the notification name when the RWVideoPlayerViewController current time has changed.
extern NSString *const RWAVPlayerViewControllerCurrentTimeChangedNotification;

/// Defines the key for the notification sent for the value of the current time as 'time'.
extern NSString *const RWAVPlayerViewControllerTimeKey;

/// Defines the key for the notification sent for the value of the duration as 'duration'.
extern NSString *const RWAVPlayerViewControllerDurationKey;

/// Defines the key for the notification sent for the value of the loaded ranges as 'ranges'.
extern NSString *const RWAVPlayerViewControllerRangesKey;

/// The RWVideoPlayerViewDelegate defines messages that sent after the video status has changed and after the video has finished playing.
@protocol RWVideoPlayerViewDelegate <NSObject>

@required
/**
 * Performs actions once the video status has changed. The new status is being sent.
 *
 * @param status The video player new status.
*/
- (void)didVideoStatusChange:(AVPlayerStatus)status;

/**
 * Performs actions once the playback has finished.
*/
- (void)didPlaybackFinish;

/**
 * Sends a message after the playback has stalled.
*/
- (void)didPlaybackStall;

/**
 * Performs actions after a new time range has been loaded.
 *
 * @param rangesInfo The loaded ranges.
*/
- (void)didLoadTimeRanges:(NSDictionary *)rangesInfo;

/**
 * Performs actions after the player frame gets tapped.
 */
- (void)didTapOnPlayerView;

@end

/**
 * A RWVideoPlayerViewController provides an AVPlayer capable of playing video. It exposes the methods to play, pause, seek forwards and backwards, get the current time, and the duration of the video.
*/
@interface RWVideoPlayerViewController : UIViewController

/// The URL pointing to the video to be played.
@property (strong, nonatomic) NSURL *url;

/// The delegate that sends messages to a video player view delegate.
@property (weak, nonatomic) id<RWVideoPlayerViewDelegate> delegate;

/// The current playback time of the video.
@property (assign, nonatomic) float currentPlaybackTime;

/**
 * Plays the video.
*/
- (void)play;

/**
 * Pauses the video.
*/
- (void)pause;

/**
 * Stops the video.
*/
- (void)stop;

/**
 * Seeks to an specified time in milliseconds as a float number. If the time is out of bounds, it does nothing.
 *
 * @param playbackTime The playback time to seek to.
*/
- (void)seekTo:(float)playbackTime;

/**
 * Seeks backwards one frame.
*/
- (void)seekBackwards;

/**
 * Seeks backwards one frame, and executed the completion block after.
 *
 * @param completion The block to be executed after seeking has finished.
*/
- (void)seekBackwardsWithCompletion:(void(^)(void))completion;

/**
 * Seeks forwards one frame.
*/
- (void)seekForwards;

/**
 * Seeks forwards one frame.
 *
 * @param completion The block to be executed after seeking has finished.
 */
- (void)seekForwardsWithCompletion:(void(^)(void))completion;

/**
 * Seeks backwards the specified number of frames.
 *
 * @param numberOfFrames The number of frames to seek backwards.
 */
- (void)seekBackwardsNumberOfFrames:(NSInteger)numberOfFrames;

/**
 * Seeks backwards the specified number of frames., and executed the completion block after.
 *
 * @param numberOfFrames The number of frames to seek backwards.
 * @param completion The block to be executed after seeking has finished.
 */
- (void)seekBackwardsNumberOfFrames:(NSInteger)numberOfFrames WithCompletion:(void(^)(void))completion;

/**
 * Seeks forwards the specified number of frames.
 *
 * @param numberOfFrames The number of frames to seek forwards.
 */
- (void)seekForwardsNumberOfFrames:(NSInteger)numberOfFrames;

/**
 * Seeks forwards the specified number of frames., and executed the completion block after.
 *
 * @param numberOfFrames The number of frames to seek forwards.
 * @param completion The block to be executed after seeking has finished.
 */
- (void)seekForwardsNumberOfFrames:(NSInteger)numberOfFrames WithCompletion:(void(^)(void))completion;

/**
 * Returns the video duration.
 */
- (float)videoDuration;

@end
