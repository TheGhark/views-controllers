//
//  RWDrawingOverlayViewController.m
//  FirstCut Pro
//
//  Created by Camilo Rodriguez Gaviria on 11/7/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import "RWDrawingOverlayViewController.h"

@interface RWDrawingOverlayViewController ()

/// A flag that indicates whether something has been drawn on the canvas.
@property (assign, nonatomic) BOOL hasChanges;

/// The last point that was touched when drawing.
@property (assign, nonatomic) CGPoint lastPoint;

/// The brush red color value.
@property (assign, nonatomic, readonly) CGFloat redColorComponent;

/// The brush green color value.
@property (assign, nonatomic, readonly) CGFloat greenColorComponent;

/// The brush blue color value.
@property (assign, nonatomic, readonly) CGFloat blueColorComponent;

/// The brush alpha color value.
@property (assign, nonatomic, readonly) CGFloat alphaColorComponent;

@end

@implementation RWDrawingOverlayViewController

#pragma mark - Life cycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSAssert(self.view, @"%@ needs its property view to be set.", [[self class] description]);
    NSAssert(self.canvas, @"%@ needs its property canvas to be set. When setting the canvas, just create an empty UIImageView. The controller will resize it to match its root view.", [[self class] description]);
    [self changeColor:[UIColor redColor]];
    self.canvas.backgroundColor = [UIColor clearColor];
}

- (void)setCanvas:(UIImageView *)canvas {
    [_canvas removeFromSuperview];
    
    canvas.frame = CGRectMake(CGRectGetMinX(self.view.frame), CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    _canvas = canvas;
    [self.view addSubview:_canvas];
}

#pragma mark - UIResponder Override

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.lastPoint = [[touches anyObject] locationInView:self.view];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self.view];

    UIGraphicsBeginImageContext(self.view.frame.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_canvas.image drawInRect:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(context, 2.0);
    CGContextSetRGBStrokeColor(context, _redColorComponent, _greenColorComponent, _blueColorComponent, _alphaColorComponent);
    CGContextMoveToPoint(context, _lastPoint.x, _lastPoint.y);
    CGContextAddLineToPoint(context, currentPoint.x, currentPoint.y);
    CGContextStrokePath(context);
    CGContextFlush(context);
    
    _canvas.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.lastPoint = currentPoint;
    self.hasChanges = YES;
}

#pragma mark - Public

- (void)changeColor:(UIColor *)color {
    CGColorRef colorRef = [color CGColor];
    NSInteger numComponents = CGColorGetNumberOfComponents(colorRef);
    
    if (numComponents == 4) {
        const CGFloat *components = CGColorGetComponents(colorRef);
        _redColorComponent = components[0];
        _greenColorComponent = components[1];
        _blueColorComponent = components[2];
        _alphaColorComponent = components[3];
    }
}

- (UIImage *)overlayImage {
    return self.canvas.image;
}

- (void)replaceDrawnImage:(UIImage *)image {
    self.canvas.image = image;
    [self.canvas setNeedsDisplay];
    self.hasChanges = YES;
}

#pragma mark - Setters/Getters

- (void)setDrawingEnabled:(BOOL)drawingEnabled {
    _drawingEnabled = drawingEnabled;
    self.view.userInteractionEnabled = drawingEnabled;
}

@end
