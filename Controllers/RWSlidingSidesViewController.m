//
//  RWSlidingSidesViewController.m
//  FirstCut Pro
//
//  Created by Camilo Rodriguez Gaviria on 11/13/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import "RWSlidingSidesViewController.h"
#import "FCViewingRoomViewController.h"

float const kOffsetForSliders = 500;
float const kAnimationInterval = 0.3;

@interface RWSlidingSidesViewController ()

/// The left toolbar. Whenever  it is tapped, the left sliding view will appear if 'shouldLeftToolbarSlide' has been set.
@property (strong, nonatomic) UIView *leftToolbar;

/// The right toolbar. Whenever  it is tapped, the left sliding view will appear if 'shouldRightToolbarSlide' has been set.
@property (strong, nonatomic) UIView *rightToolbar;

/// A flag that indicates whether the maximum height and the x origin have been set for the content view.
@property (assign, nonatomic) BOOL didSetContentViewConstraints;

/// The maximum width for the content view. Read only property that is set when the view is loaded.
@property (assign, nonatomic, readonly) float contentViewMaxWidth;

/// The x origin for the content view. Read only property that is set when the view is loaded.
@property (assign, nonatomic, readonly) float contentViewMinX;

/// The left tap gesture recognizer that slides the left view if 'shouldLeftToolbarSlide' has been set.
@property (strong, nonatomic) UITapGestureRecognizer *leftTapGestureRecognizer;

/// The left tap gesture recognizer that slides the right view if 'shouldRightToolbarSlide' has been set.
@property (strong, nonatomic) UITapGestureRecognizer *rightTapGestureRecognizer;

/**
 * Resize the content view. It is been called after sliding either the left or the right views.
 *
 * @param animated Indicates whether the resizing should be animated.
*/
- (void)resizeContentViewAnimated:(BOOL) animated;

@end

@implementation RWSlidingSidesViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.leftTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideLeftSlidingView)];
    self.rightTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideRightSlidingView)];
    
    self.shouldLeftToolbarSlide = YES;
    self.shouldRightToolbarSlide = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!self.didSetContentViewConstraints) {
        _contentViewMaxWidth = CGRectGetWidth(self.contentView.bounds);
        _contentViewMinX = self.contentView.frame.origin.x;
        self.didSetContentViewConstraints = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setters

- (void)setShouldLeftToolbarSlide:(BOOL)shouldLeftToolbarSlide {
    _shouldLeftToolbarSlide = shouldLeftToolbarSlide;
    
    if (_shouldLeftToolbarSlide) {
        [self.leftToolbar addGestureRecognizer:self.leftTapGestureRecognizer];
    } else {
        [self.leftToolbar removeGestureRecognizer:self.leftTapGestureRecognizer];
    }
}

- (void)setShouldRightToolbarSlide:(BOOL)shouldRightToolbarSlide {
    _shouldRightToolbarSlide = shouldRightToolbarSlide;
    
    if (_shouldRightToolbarSlide) {
        [self.rightToolbar addGestureRecognizer:self.rightTapGestureRecognizer];
    } else {
        [self.rightToolbar removeGestureRecognizer:self.rightTapGestureRecognizer];
    }
}

#pragma mark - Private

- (void)resizeContentViewAnimated:(BOOL)animated {
    float maxWidth = self.contentViewMaxWidth;
    float xOrigin = self.contentViewMinX;
    float leftSlidingViewOrigin = CGRectGetMaxX(self.leftSlidingView.frame);
    
    if (self.leftSlidingViewVisible) {
        maxWidth -= CGRectGetWidth(self.leftSlidingView.frame) - CGRectGetWidth(self.leftToolbar.frame);
        
        if (xOrigin < leftSlidingViewOrigin) {
            xOrigin = CGRectGetMaxX(self.leftSlidingView.frame);
        }
    }
    
    if (self.rightSlidingViewVisible) {
        maxWidth -= CGRectGetWidth(self.rightSlidingView.frame) - CGRectGetWidth(self.rightToolbar.frame);
    }
    if(animated) {
        [UIView animateWithDuration:kAnimationInterval animations:^{
            self.contentView.frame = CGRectMake(xOrigin, CGRectGetMinY(self.contentView.frame), maxWidth, CGRectGetHeight(self.contentView.frame));
        }];
    } else {
            self.contentView.frame = CGRectMake(xOrigin, CGRectGetMinY(self.contentView.frame), maxWidth, CGRectGetHeight(self.contentView.frame));
    }
}

#pragma mark - Public

- (void)removeLeftSlidingViewSubviews {
    [self.leftSlidingView.subviews each:^(UIView *subview) {
        [subview removeFromSuperview];
    }];
}

- (void)removeRightSlidingViewSubviews {
    [self.rightSlidingView.subviews each:^(UIView *subview) {
        [subview removeFromSuperview];
    }];
}

- (void)slideLeftSlidingViewAnimated: (BOOL) animated {
    if (self.delegate) {
        [self.delegate willSlideLeftSlidingView];
    }
    
    float xOrigin = 0;
    
    if (self.leftSlidingViewVisible) {
        xOrigin = CGRectGetMinX(self.view.frame) - kOffsetForSliders;
        self.leftSlidingViewVisible = NO;
    } else {
        xOrigin = 0;
        self.leftSlidingViewVisible = YES;
    }
    if(animated) {
        [UIView animateWithDuration:kAnimationInterval animations:^{
            self.leftSlidingView.frame = CGRectMake(xOrigin, CGRectGetMinY(self.leftSlidingView.frame), CGRectGetWidth(self.leftSlidingView.frame), CGRectGetHeight(self.leftSlidingView.frame));
            [self resizeContentViewAnimated:animated];
        } completion:^(BOOL finished) {
            if (self.delegate) {
                [self.delegate didSlideLeftSlidingView];
            }
        }];
        
    }
    else {
            self.leftSlidingView.frame = CGRectMake(xOrigin, CGRectGetMinY(self.leftSlidingView.frame), CGRectGetWidth(self.leftSlidingView.frame), CGRectGetHeight(self.leftSlidingView.frame));
            [self resizeContentViewAnimated:animated];
            if (self.delegate) {
                [self.delegate didSlideLeftSlidingView];
            }
    }
}
- (void) slideLeftSlidingView
{
    [self slideLeftSlidingViewAnimated:YES];
}

- (void) slideRightSlidingView
{
    [self slideRightSlidingViewAnimated:YES];
}

- (void)slideRightSlidingViewAnimated:(BOOL) animated {
    if (self.delegate) {
        [self.delegate willSlideRightSlidingView];
    }
    
    float xOrigin = 0;
    
    if (self.rightSlidingViewVisible) {
        xOrigin = CGRectGetMaxY(self.view.frame);
        self.rightSlidingViewVisible = NO;
    } else {
        xOrigin = CGRectGetMaxY(self.view.frame) - CGRectGetWidth(self.rightSlidingView.frame) - CGRectGetWidth(self.rightToolbar.frame);
        self.rightSlidingViewVisible = YES;
    }
    if(animated) {
        [UIView animateWithDuration:kAnimationInterval animations:^{
            self.rightSlidingView.frame = CGRectMake(xOrigin, CGRectGetMinY(self.rightSlidingView.frame), CGRectGetWidth(self.rightSlidingView.frame), CGRectGetHeight(self.rightSlidingView.frame));
            [self resizeContentViewAnimated:animated];
        } completion:^(BOOL finished) {
            if (self.delegate) {
                [self.delegate didSlideRightSlidingView];
            }
        }];
    } else {
        self.rightSlidingView.frame = CGRectMake(xOrigin, CGRectGetMinY(self.rightSlidingView.frame), CGRectGetWidth(self.rightSlidingView.frame), CGRectGetHeight(self.rightSlidingView.frame));
        [self resizeContentViewAnimated:animated];
        if (self.delegate) {
            [self.delegate didSlideRightSlidingView];
        }
    }
}

@end
