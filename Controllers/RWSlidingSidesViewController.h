//
//  RWSlidingSidesViewController.h
//  FirstCut Pro
//
//  Created by Camilo Rodriguez Gaviria on 11/13/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import <UIKit/UIKit.h>

/// The FCSlidingSidesDelegate protocol defines the messages sent to a sliding sides controller delegate before and after sliding the views.
@protocol RWSlidingSidesDelegate <NSObject>

@required
/**
 * Perfom actions before the left sliding view starts to be animated
 */
- (void)willSlideLeftSlidingView;
/**
 * Perfom actions before the right sliding view starts to be animated
 */
- (void)willSlideRightSlidingView;
/**
 * Perfom actions after the left sliding view starts to be animated
 */
- (void)didSlideLeftSlidingView;
/**
 * Perfom actions after the right sliding view starts to be animated
 */
- (void)didSlideRightSlidingView;

@end

/**
 * A RWSlidingSidesViewController provides a controller with two sliding views from both the left and the right sides.
*/
@interface RWSlidingSidesViewController : UIViewController

/// The main view of the controller. Everthing that is inside, will get resized after animations
@property (strong, nonatomic) UIView *contentView;

/// The left side sliding view.
@property (strong, nonatomic) UIView *leftSlidingView;

/// The right side sliding view.
@property (strong, nonatomic) UIView *rightSlidingView;
/// A flag that indicates whether the left sliding view should slide.
@property (assign, nonatomic) BOOL shouldLeftToolbarSlide;

/// A flag that indicates whether the right sliding view should slide.
@property (assign, nonatomic) BOOL shouldRightToolbarSlide;

/// The class delegate which will send messages before and after sliding the either the left or right slide views.
@property (weak, nonatomic) id<RWSlidingSidesDelegate> delegate;

/// A flag that indicates is the left sliding view should be visible at all times.
@property (assign, nonatomic) BOOL leftSlidingViewVisible;

/// A flag that indicates is the left sliding view should be visible at all times.
@property (assign, nonatomic) BOOL rightSlidingViewVisible;

/**
 * Removes all subviews from the left sliding view. Useful when changing its content.
*/
- (void)removeLeftSlidingViewSubviews;

/**
 * Removes all subviews from the right sliding view. Useful when changing its content.
 */
- (void)removeRightSlidingViewSubviews;

/**
 * Slides the left view. Animation is optional.
 * @param animated Indicates whether the sliding should be animated.
 */
- (void)slideLeftSlidingViewAnimated: (BOOL) animated;

/**
 * Slides the right view. Animation is optional.
 * @param animated Indicates whether the sliding should be animated.
 */
- (void)slideRightSlidingViewAnimated: (BOOL) animated;

@end
