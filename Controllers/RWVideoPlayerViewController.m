//
//  RWViewController.m
//  VideoPlayer
//
//  Created by Camilo Rodriguez Gaviria on 12/4/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import "RWVideoPlayerViewController.h"

NSString *const RWAVPlayerViewControllerCurrentTimeChangedNotification = @"RWAVPlayerViewControllerCurrentTimeChangedNotification";
NSString *const RWAVPlayerViewControllerTimeKey = @"time";
NSString *const RWAVPlayerViewControllerDurationKey = @"duration";
NSString *const RWAVPlayerViewControllerRangesKey = @"ranges";

/// Defines the interval in seconds to update the current playback time.
NSInteger const RWAVPlayerViewControllerPollingIntervalTime = 1;

/// Defines the timescale for the playback time calculations.
NSInteger const kTimescale = 10000;

@interface RWVideoPlayerViewController ()

/// The view that holds the player.
@property (strong, nonatomic) IBOutlet RWAVPlayerView *playerView;

/// The assests player.
@property (strong, nonatomic) AVPlayer *player;

/// The asset to be played.
@property (strong, nonatomic) AVPlayerItem *playerItem;

/// The periodic time observer added to track time changes in the video.
@property (strong, nonatomic) id periodicTimeObserver;

/// The polling timer for the video player.
@property (strong, nonatomic) NSTimer *pollingTimer;

/**
 * Handles the AVPlayerItemDidPlayToEndTimeNotification.
*/
- (void)itemDidFinishPlaying;

/**
 * Calls the delegate 'didLoadTimeRanges' method after a new time range has been loaded.
*/
- (void)updateLoadedRanges;

/**
 * Handles the RWAVPlayerViewControllerCurrentTimeChangedNotification.
 *
 * @param note The notification containing the object.
*/
- (void)currentPlaybackTimeChange:(NSNotification *)note;

/**
 * Handles the AVPlayerItemPlaybackStalledNotification.
*/
- (void)itemDidStall:(NSNotification *)note;

/**
 * Pauses the playback when the video gets tapped and no drawing is happening.
 *
 * @param recognizer The tap gesture recognizer that is being executed.
*/
- (void)pauseOnTap:(UITapGestureRecognizer *)recognizer;

@end

@implementation RWVideoPlayerViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSAssert(self.playerView && [self.playerView isKindOfClass:[RWAVPlayerView class]], @"The player view is invalid. It is an instance of a %@, but it was expected to be an instance of a %@", [[self.playerView class] description], [[RWAVPlayerView class] description]);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying) name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(currentPlaybackTimeChange:) name:RWAVPlayerViewControllerCurrentTimeChangedNotification object:self.playerItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidStall:) name:AVPlayerItemPlaybackStalledNotification object:self.playerItem];
}

#pragma mark - Setters

- (void)setUrl:(NSURL *)url {
    if (!url) {
#ifdef DEV_OPS
        url = [NSURL URLWithString:@"https://dl.dropboxusercontent.com/s/7x6vltc8akwaqi6/2012-07-13%2017.47.48.mp4"];
        NSLog(@"**** No URL was given to %@ ****. The url property has been set with:\n%@", [[self class] description], self.url);
#else
        NSLog(@"**** No URL was given to %@ ****. Please set the url by changing the value of the property url.", [[self class] description]);
#endif
    }
    
    _url = url;
    self.playerItem = [[AVPlayerItem alloc] initWithURL:_url];
    self.player = [[AVPlayer alloc] initWithPlayerItem:self.playerItem];
    [self.playerView setPlayer:self.player];
    [self.playerView setExternalPlayback:NO];
    
    __block AVPlayer *player = self.player;
    __block float duration = -1;
    
    [self.player addPeriodicTimeObserverForInterval:CMTimeMake(1, kTimescale) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        float currentTime = CMTimeGetSeconds(time);
        duration = (duration == -1) ? CMTimeGetSeconds(player.currentItem.duration) : duration;

        NSDictionary *userInfo = @{RWAVPlayerViewControllerTimeKey: [NSString stringWithFormat:@"%f", currentTime],
                                   RWAVPlayerViewControllerDurationKey: [NSString stringWithFormat:@"%f", duration]};
        
        [[NSNotificationCenter defaultCenter] postNotificationName:RWAVPlayerViewControllerCurrentTimeChangedNotification object:player.currentItem userInfo:userInfo];
    }];
    
    [self.player addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionInitial context:nil];

    self.pollingTimer = [NSTimer scheduledTimerWithTimeInterval:RWAVPlayerViewControllerPollingIntervalTime target:self selector:@selector(updateLoadedRanges) userInfo:nil repeats:YES];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pauseOnTap:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)setPollingTimer:(NSTimer *)pollingTimer {
    if (_pollingTimer) {
        [_pollingTimer invalidate];
    }
    
    _pollingTimer = pollingTimer;
}

#pragma mark - Observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"status"]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didVideoStatusChange:)]) {
            [self.delegate didVideoStatusChange:self.player.status];
        } else {
            NSLog(@"The video status changed. However, no delegate was defined.");
        }
    }
}

#pragma mark - Private

- (void)itemDidFinishPlaying {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didPlaybackFinish)]) {
        [self.delegate didPlaybackFinish];
    } else {
        NSLog(@"The video has finshed playing. However, no delegate was defined.");
    }
}

- (void)updateLoadedRanges {
    FCDelegateAssert(self.delegate, self);
    NSArray *loadedRanges = [self.player.currentItem loadedTimeRanges];
    
    if (loadedRanges) {
        NSDictionary *rangesInfo = @{RWAVPlayerViewControllerRangesKey: [self.player.currentItem loadedTimeRanges],
                                     RWAVPlayerViewControllerDurationKey : [NSString stringWithFormat:@"%f", CMTimeGetSeconds(self.player.currentItem.duration)]};
        
            [self.delegate didLoadTimeRanges:rangesInfo];
    }
}

- (void)currentPlaybackTimeChange:(NSNotification *)note {
    self.currentPlaybackTime = [[[note userInfo] objectForKey:RWAVPlayerViewControllerTimeKey] floatValue];
}

- (void)itemDidStall:(NSNotification *)note {
    FCDelegateAssert(self.delegate, self);
    [self.delegate didPlaybackStall];
}

- (void)pauseOnTap:(UITapGestureRecognizer *)recognizer {
    [self pause];
    FCDelegateAssert(self.delegate, self);
    [self.delegate didTapOnPlayerView];
}

#pragma mark - Public

- (void)play {
    [self.player play];
}

- (void)pause {
    [self.player pause];
}

- (void)stop {
    [self.player seekToTime:CMTimeMake(0, kTimescale)];
    [self.player pause];
    [self.pollingTimer invalidate];
}

- (void)seekTo:(float)playbackTime {
    CMTime time = CMTimeMakeWithSeconds(playbackTime, kTimescale);
    [self.player seekToTime:time];
}

- (void)seekBackwards {
    if ([self.player.currentItem canStepBackward]) {
        [self.player.currentItem stepByCount:-1];
    }
}

- (void)seekBackwardsWithCompletion:(void (^)(void))completion {
    [self seekBackwards];
    
    if (completion) {
        completion();
    }
}

- (void)seekForwards {
    if ([self.player.currentItem canStepForward]) {
        [self.player.currentItem stepByCount:1];
    }
}

-(void)seekForwardsWithCompletion:(void (^)(void))completion {
    [self seekForwards];
    
    if (completion) {
        completion();
    }
}

- (void)seekBackwardsNumberOfFrames:(NSInteger)numberOfFrames {
    if ([self.player.currentItem canStepBackward]) {
        [self.player.currentItem stepByCount:-numberOfFrames];
    }
}

- (void)seekBackwardsNumberOfFrames:(NSInteger)numberOfFrames WithCompletion:(void (^)(void))completion {
    [self seekBackwardsNumberOfFrames:numberOfFrames];
    
    if (completion) {
        completion();
    }
}

- (void)seekForwardsNumberOfFrames:(NSInteger)numberOfFrames {
    if ([self.player.currentItem canStepForward]) {
        [self.player.currentItem stepByCount:numberOfFrames];
    }
}

- (void)seekForwardsNumberOfFrames:(NSInteger)numberOfFrames WithCompletion:(void (^)(void))completion {
    [self seekBackwardsNumberOfFrames:numberOfFrames];
    
    if (completion) {
        completion();
    }
}

- (float)videoDuration {
    return CMTimeGetSeconds(self.player.currentItem.duration);
}

#pragma mark - Dealloc

- (void)dealloc {
    FCDeallocLog(self);
    [self.pollingTimer invalidate];
    [self.player removeObserver:self forKeyPath:@"status" context:nil];
    [self.player removeTimeObserver:self.periodicTimeObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RWAVPlayerViewControllerCurrentTimeChangedNotification object:self.playerItem];
}

@end