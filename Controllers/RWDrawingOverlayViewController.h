//
//  RWDrawingOverlayViewController.h
//  FirstCut Pro
//
//  Created by Camilo Rodriguez Gaviria on 11/7/13.
//  Copyright (c) 2013 Railwaymen. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * A RWDrawingOverlayViewController provies a controller with a white canvas allowing to draw on it. The default brush color is red.
*/
@interface RWDrawingOverlayViewController : UIViewController

/// The image view that repesents the empty canvas
@property (strong, nonatomic) IBOutlet UIImageView *canvas;

/// Enables drawing
@property(assign, nonatomic) BOOL drawingEnabled;

/**
 * Changes the canvas painting color. Default value is red.
*/
- (void)changeColor:(UIColor *)color;

/**
 * Returns the drawn image.
 *
 * @return overlayImage The image attribute of the canvas property.
*/
- (UIImage *)overlayImage;

/**
 * Replaces the current image with a new one.
 *
 * @param image The new image to be set on the canvas.
*/
-(void)replaceDrawnImage:(UIImage *)image;

@end
